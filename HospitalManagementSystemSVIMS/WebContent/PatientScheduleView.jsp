<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.svims.model.Patient"%>
<%@ page import="com.svims.model.Doctor"%>
<%@ page import="java.util.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
#firstDiv{
	background-color: lightgrey;
  	width: 1000px;
  	border: 25px solid green;
  	padding: 25px;
  	margin: 25px;
}
</style>
</head>
<body>
	<%
		//ArrayList<Object> objectList =(ArrayList)request.getAttribute("objectList");
		//Patient patient = (Patient)objectList.get(0);
		//Doctor doctor = (Doctor)objectList.get(1);

		ArrayList<Patient> arrayList = (ArrayList) request.getAttribute("patientList");
		//out.println("in jsp");
		//out.println(arrayList.get(0).getPatientId());
		//out.println(arrayList.get(0).getDepartment());
		// out.println(arrayList.get(0).getPatientDate());
		//out.println(arrayList.get(0).getPatientName());

		//RequestDispatcher reqDispatcher = request.getRequestDispatcher("Admin.html");
		//response.getWriter().println("enter login id or password");
		//reqDispatcher.include(request, response);
		//I need to send department here
		//for (int i = 0; i < arrayList.size(); i++) {
			//request.setAttribute("pid",arrayList.get(i).getPatientId());
			//request.setAttribute("department", arrayList.get(0).getDepartment());
			//will it go like that or I need to use URL rewriting
			//Patient newPatient = new Patient();
			//newPatient.setPatientId(arrayList.get(i).getPatientId());
			//newPatient.setDepartment(arrayList.get(i).getDepartment());
			//out.println("in jsp page");
			//out.println(arrayList.get(i).getPatientId());
			//out.println(arrayList.get(i).getDepartment());
			//request.setAttribute("pid",arrayList.get(i).getPatientId());
			//request.setAttribute("department", arrayList.get(i).getDepartment());
			//out.println("inside js: getAttribute");
			//out.println(request.getAttribute("pid"));
			//out.println(request.getAttribute("department"));
			//session.setAttribute("pid", arrayList.get(i).getPatientId());
			//session.setAttribute("department", arrayList.get(i).getDepartment());
			//request.setAttribute("name", "Vinusha");
			//session.setAttribute("reqObj", request);
			
	%>
	
		
<div id ="firstDiv">
<div class="container"> 

<table class="table">
    <thead>
      <tr>
        <th>Patient Id</th>
        <th>Patient Name</th>
        <th>Department</th>
        <th></th>
      </tr>
    </thead>
  </table>


	<% for (int i = 0; i < arrayList.size(); i++) {
	   Patient patient = new Patient();
		patient.setPatientId(arrayList.get(i).getPatientId());
		patient.setDepartment(arrayList.get(i).getDepartment());
   
   %>
   <form action="patientScheduleFinalAction">   
   <table class = "table">    
    <tbody>
    
      <tr>
      
        <td><%
						out.println(arrayList.get(i).getPatientId());
					%></td>
        <td><%
						out.println(arrayList.get(i).getPatientName());
					%></td>
        <td><%
						out.println(arrayList.get(i).getDepartment());
					%></td>
         <td><input type = "hidden" name ="patient_id" value = "<%=patient.getPatientId()%>"></td>
				<td><input type = "hidden" name ="patient_dept" value = "<%=patient.getDepartment()%>"></td>
				
				<td><button type="submit">Submit</button></td>  
      </tr>
      
      
    </tbody>
  </table>
  </form>
  <%
		}
	%>
</div>
</div>

	
</body>
</html>