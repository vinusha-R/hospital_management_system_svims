<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "com.svims.model.Report" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
#firstDiv{
	background-color: lightgrey;
  	width: 1000px;
  	border: 25px solid green;
  	padding: 25px;
  	margin: 25px;
}
</style>
</head>
<body>
<% Report report =(Report)request.getAttribute("finalReport");
//out.println(report.isDiagnosed());
%>
<%-- <table><tr><th>Report Id</th><th>Diagnosed</th><th>PBS</th><th>CBCS</th><th>Antigen</th><th>Patient Id</th></tr>
<tr><td><%out.println(report.getReportId());%></td><td><%out.println(report.isDiagnosed()); %></td><td><%out.println(report.getPbs());%></td><td><%out.println(report.getCbcs());%></td><td><%out.println(report.getAntigen());%></td><td><%out.println(report.getReportPid());%></td></tr>
</table>--%>


<div id ="firstDiv">
<div class="container">           
  <table class="table">
    <thead>
      <tr>
        <th>Report Id</th>
        <th>Diagnosed</th>
        <th>PBS</th>
        <th>CBCS</th>
        <th>Antigen</th>
        <th>Patient Id</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><%=report.getReportId()%></td>
        <td><%=report.isDiagnosed()%></td>
        <td><%=report.getPbs()%></td>
         <td><%=report.getCbcs()%></td>
          <td><%=report.getAntigen()%></td>
           <td><%=report.getReportPid()%></td>  
      </tr>
    </tbody>
  </table>
</div>
</div>
</body>
</html>