package com.svims.doctor;

import java.util.ArrayList;

import com.svims.UserAccount;
import com.svims.dao.DoctorDao;
import com.svims.model.Doctor;

public class DoctorAccount implements UserAccount {
	DoctorDao doctorDao = new DoctorDao();
	Doctor doctors[] = new Doctor[10];

	@Override
	public boolean login(String doctorId, String doctorPassword) {
		// TODO Auto-generated method stub
		// ArrayList<String> idList = new ArrayList<>();

		String password = null;

		ArrayList<String> idList = doctorDao.readDoctor("doctor");

		if (idList.contains(doctorId)) {
			Doctor doctor = doctorDao.readDoctor("doctor", doctorId);
			password = doctor.getDocPwd();
		} else {
			return false;
		}
		if (password.equals(doctorPassword)) {
			System.out.println("welcome");
			return true;
		} else {
			System.out.println("invalid password");
			return false;
		}
	}

	@Override
	public boolean register(Object object) {
		Doctor doctor = (Doctor) object;
		String doctorName = doctor.getDocName();
		String doctorPassword = doctor.getDocPwd();
		String specialization = doctor.getSpecialization();
		String doctorDate = doctor.getDocDate();
		System.out.println(doctorDate);

		// String date = "3/31/2019";
		// String[] words=s1.split("\\s");//splits the string based on whitespace
		String[] delimitedDate = doctorDate.split("/");
		System.out.println(delimitedDate[0]);
		String newDoctorDate = delimitedDate[2] + "-" + delimitedDate[0] + "-" + delimitedDate[1];
		System.out.println(newDoctorDate);

		boolean patientChecked = false;
		int id = generateId("doctor");
		String newId = "d" + id;
		boolean status = doctorDao.createDoctor(newId, doctorName, doctorPassword, specialization, newDoctorDate,
				patientChecked);
		return status;
	}

	@Override
	public int generateId(String tableName) {
		ArrayList<String> idList = doctorDao.readDoctor(tableName);
		if (idList.isEmpty()) {
			return 1;
		} else {
			String id = idList.get(idList.size() - 1);
			int newId = Integer.parseInt(id.substring(1));
			return ++newId;
		}
	}
}
