package com.svims.doctor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.svims.dao.DoctorDao;
import com.svims.dao.PatientDao;
import com.svims.dao.ReportDao;
import com.svims.model.Doctor;
import com.svims.model.Patient;
import com.svims.model.Report;

public class DoctorOperations {
	PatientDao patientDao = new PatientDao();
	DoctorDao doctorDao = new DoctorDao();
	ReportDao reportDao = new ReportDao();

	public ArrayList<Patient> viewPatientList(String doctorId) {
		// Patient patients[] = new Patient[10];
		LocalDate todayDate = java.time.LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = (todayDate).format(formatter);

		ArrayList<Patient> patientList = patientDao.readPatientList(doctorId, formattedDate);

		return patientList;
	}

	public boolean checkPatient(String patientId) {

		boolean status = doctorDao.patientCheckup(patientId);
		return status;
	}

	public boolean generatePatientReport(String isDiagnosed, String pbs, String cbcs, String antigen,
			String patientId) {

		int newId = generateReportId(patientId);
		String reportId = "R" + newId;
		boolean status = reportDao.createReport(reportId, isDiagnosed, pbs, cbcs, antigen, patientId);
		return status;
	}

	public Report viewPatientReport(String reportId) {
		Report report = reportDao.readReport(reportId);
		return report;
	}

	public int generateReportId(String patientId) {
		Report reports[] = new Report[10];
		reports = reportDao.readReport();
		ArrayList<String> reportIdList = new ArrayList<>();
		if (reports == null) {
			return 1;
		} else {
//			for(Doctor doctor: doctors) {
//				doctorIdList.add(doctor.getDocId());
//			}
//			

			for (int i = 0; i < reports.length; i++) {
				if (reports[i] != null) {
					System.out.println(reports[i]);
					reportIdList.add(reports[i].getReportId());

				}

			}

			String id = reportIdList.get(reportIdList.size() - 1);
			int newId = Integer.parseInt(id.substring(1));
			return ++newId;
		}
	}
}
