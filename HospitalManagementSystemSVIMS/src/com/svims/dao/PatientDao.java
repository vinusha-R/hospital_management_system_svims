package com.svims.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import com.svims.database.DatabaseConnection;
import com.svims.model.Patient;

public class PatientDao {
	Connection connection = DatabaseConnection.connectDatabase();

	public boolean createPatient(String newId, String patientName, String patientDept, String docid,
			String patientPassword, String patientDate, boolean status, boolean checkupDone) {

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					"insert into patient(pid,pname,department,docid,ppwd,pdate,status,checkupDone)values(?,?,?,?,?,?,?,?)");
			preparedStatement.setString(1, newId);
			preparedStatement.setString(2, patientName);
			preparedStatement.setString(3, patientDept);
			preparedStatement.setString(4, docid);
			preparedStatement.setString(5, patientPassword);
			preparedStatement.setString(6, patientDate);
			preparedStatement.setBoolean(7, status);
			preparedStatement.setBoolean(8, checkupDone);

			int a = preparedStatement.executeUpdate();
			if (a != 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

//	public Patient[] readPatient(String tableName) {
//		Patient patients[] = new Patient[10];
//		int i=0;
//		try {
//			Statement statement = connection.createStatement();
//			ResultSet resultSet = statement.executeQuery("select * from "+tableName);
//			while(resultSet.next()) {
//				patients[i] = new Patient();
//				patients[i].setPatientId(resultSet.getString(1));
//				patients[i].setPatientName(resultSet.getString(2));
//				patients[i].setDepartment(resultSet.getString(3));
//				patients[i].setPatientPwd(resultSet.getString(5));
//				patients[i].setPatientDate(resultSet.getString(6));
//				patients[i].setPatientStatus(resultSet.getBoolean(7));
//				patients[i].setCheckUpDone(resultSet.getBoolean(8));
//				i++;
//			}
//			
//			return patients;
//		}catch(SQLException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}

	public ArrayList<String> readPatient(String tableName) {
		ArrayList<String> idList = new ArrayList<>();
		int i = 0;
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from " + tableName);
			while (resultSet.next()) {
				idList.add(resultSet.getString(1));
				i++;
			}
			return idList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Patient readPatient(String tableName, String patientId) {
		Patient patient = new Patient();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("select * from " + tableName + " where pid = '" + patientId + "'");
			resultSet.next();
			System.out.println(resultSet.getString(5));
			patient.setPatientId(resultSet.getString(1));
			patient.setPatientName(resultSet.getString(2));
			patient.setDepartment(resultSet.getString(3));
			patient.setPatientPwd(resultSet.getString(5));
			patient.setPatientDate(resultSet.getString(6));
			patient.setPatientStatus(resultSet.getBoolean(7));
			patient.setCheckUpDone(resultSet.getBoolean(8));
			return patient;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList readPatientList(String doctorId, String date) {
		// Patient patients[] = new Patient[10];
		ArrayList<Patient> patientList = new ArrayList<>();
		int i = 0;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					"select * from patient where docid = ? and pdate = ? and status = ? and checkupDone = ?");
			System.out.println(doctorId);
			preparedStatement.setString(1, doctorId);
			preparedStatement.setString(2, date);
			// status.....patient being confirmed with doctor available date
			preparedStatement.setBoolean(3, true);
			preparedStatement.setBoolean(4, false);
			// preparedStatement.setBoolean(4, false);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				resultSet.beforeFirst();
				while (resultSet.next()) {
					Patient patient = new Patient();
					System.out.println(resultSet.getString(2));
					System.out.println(resultSet.getString(3));
					System.out.println(resultSet.getString(4));
					patient.setPatientId(resultSet.getString(1));
					patient.setPatientName(resultSet.getString(2));
					patient.setDepartment(resultSet.getString(3));
					patient.setPatientPwd(resultSet.getString(5));
					patient.setPatientDate(resultSet.getString(6));
					patient.setPatientStatus(resultSet.getBoolean(7));
					patient.setCheckUpDone(resultSet.getBoolean(8));
					patientList.add(patient);

//			patients[i] = new Patient();
//			patients[i].setPatientId(resultSet.getString(1));
//			patients[i].setPatientName(resultSet.getString(2));
//			patients[i].setDepartment(resultSet.getString(3));
//			patients[i].setPatientPwd(resultSet.getString(5));
//			patients[i].setPatientDate(resultSet.getString(6));
//			patients[i].setPatientStatus(resultSet.getBoolean(7));
//			patients[i].setCheckUpDone(resultSet.getBoolean(8));
//			i++;
				}
				return patientList;
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
