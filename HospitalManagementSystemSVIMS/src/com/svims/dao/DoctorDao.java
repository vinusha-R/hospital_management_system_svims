package com.svims.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.svims.database.DatabaseConnection;
import com.svims.model.Doctor;

public class DoctorDao {
	Connection connection = DatabaseConnection.connectDatabase();

	public boolean createDoctor(String newId, String doctorName, String doctorPassword, String specialization,
			String doctorDate, boolean patientChecked) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					"insert into doctor(docid,docName,specialization,ddate,docPwd,patientChecked)values(?,?,?,?,?,?)");
			preparedStatement.setString(1, newId);
			preparedStatement.setString(2, doctorName);
			preparedStatement.setString(3, specialization);
			preparedStatement.setString(4, doctorDate);
			preparedStatement.setString(5, doctorPassword);
			preparedStatement.setBoolean(6, patientChecked);

			int a = preparedStatement.executeUpdate();
			if (a != 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}
//	public Doctor[] readDoctor(String tableName) {
//		Doctor doctors[] = new Doctor[10];
//		int i=0;
//		try {
//			Statement statement = connection.createStatement();
//			ResultSet resultSet = statement.executeQuery("select * from "+tableName);
//			while(resultSet.next()) {
//				doctors[i] = new Doctor();
//				doctors[i].setDocId(resultSet.getString(1));
//				doctors[i].setDocName(resultSet.getString(2));
//				doctors[i].setSpecialization(resultSet.getString(3));
//				doctors[i].setDocDate(resultSet.getString(4));
//				doctors[i].setDocPwd(resultSet.getString(5));
//				doctors[i].setPatientChecked(resultSet.getBoolean(6));
//				i++;
//			}
//			
//			return doctors;
//		}catch(SQLException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}

	public ArrayList<String> readDoctor(String tableName) {
		ArrayList<String> idList = new ArrayList<>();
		int i = 0;
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from " + tableName);
			while (resultSet.next()) {
				idList.add(resultSet.getString(1));
			}
			return idList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Doctor readDoctor(String tableName, String doctorId) {
		Doctor doctor = new Doctor();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("select * from " + tableName + " where docid = '" + doctorId + "'");
			resultSet.next();
			doctor.setDocPwd(resultSet.getString(5));
			return doctor;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean patientCheckup(String patientId) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("update patient set checkupDone = ? where pid = ?");
			preparedStatement.setBoolean(1, true);
			preparedStatement.setString(2, patientId);
			int status = preparedStatement.executeUpdate();
			if (status != 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean generateReport(String patientId, String reportId, String diagnosed, String pbs, String cbcs,
			String antigen) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					"insert into patientreport(reportId,diagnosed,pbs,cbcs,antigen,pid)values(?,?,?,?,?,?)");
			preparedStatement.setString(1, reportId);
			preparedStatement.setString(2, diagnosed);
			preparedStatement.setString(3, pbs);
			preparedStatement.setString(4, cbcs);
			preparedStatement.setString(5, antigen);
			preparedStatement.setString(6, patientId);
			int status = preparedStatement.executeUpdate();
			if (status != 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

}
