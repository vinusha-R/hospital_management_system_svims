package com.svims.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.svims.database.DatabaseConnection;
import com.svims.model.Admin;
import com.svims.model.Doctor;
import com.svims.model.Patient;

public class AdminDao {
	Connection connection = DatabaseConnection.connectDatabase();

	public boolean createAdmin(String adminId, String adminName, String adminPassword) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("insert into admin(aid,aname,apwd)values(?,?,?)");
			preparedStatement.setString(1, adminId);
			preparedStatement.setString(2, adminName);
			preparedStatement.setString(3, adminPassword);
			int a = preparedStatement.executeUpdate();
			if (a != 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

//	public Admin[] readAdmin(String tableName) {
//		Admin admins[] = new Admin[10];
//		int i=0;
//		try {
//			Statement statement = connection.createStatement();
//			ResultSet resultSet = statement.executeQuery("select * from "+tableName);
//			while(resultSet.next()) {
//				admins[i] = new Admin();
//				admins[i].setAdminId(resultSet.getString(1));
//				admins[i].setAdminName(resultSet.getString(2));
//				admins[i].setAdminPwd(resultSet.getString(3));
//				i++;
//			}
//			System.out.println(admins);
//			return admins;
//		}catch(SQLException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//	public ArrayList<Admin> readAdmin(String tableName){
//		ArrayList<Admin> admins = new ArrayList<>();
//		int i =0;
//		try {
//			Statement statement = connection.createStatement();
//			ResultSet resultSet = statement.executeQuery("select * from "+tableName);
//			while(resultSet.next()) {
//				Admin admin = new Admin();
//				admin.setAdminId(resultSet.getString(1));
//				admin.setAdminName(resultSet.getString(2));
//				admin.setAdminPwd(resultSet.getString(3));
//				i++;
//				}
//			return admins;
//			}catch(SQLException e) {
//				e.printStackTrace();
//				return null;
//		}
//
//	}
	public ArrayList<String> readAdmin(String tableName) {
		ArrayList<String> idList = new ArrayList<>();
		int i = 0;
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from " + tableName);
			while (resultSet.next()) {
				idList.add(resultSet.getString(1));
			}
			return idList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Admin readAdmin(String tableName, String adminId) {
		Admin admin = new Admin();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("select * from " + tableName + " where aid = '" + adminId + "'");
			System.out.println(resultSet);
			System.out.println(adminId);
			System.out.println(tableName);
			resultSet.next();
			admin.setAdminPwd(resultSet.getString(3));
			return admin;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		}
	}

	public ArrayList<Patient> getPatientSchedule() {

		ArrayList<Patient> arrayList = new ArrayList<Patient>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("select pid,pname,department from patient where pdate IS NULL AND docid IS NULL");
			if(resultSet.next()) {
				resultSet.beforeFirst();
			while (resultSet.next()) {
				Patient patient = new Patient();
				patient.setPatientId(resultSet.getString(1));
				patient.setPatientName(resultSet.getString(2));
				patient.setDepartment(resultSet.getString(3));
				arrayList.add(patient);
			}
			return arrayList;
			}else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

//		Patient patient = new Patient();
//		Patient patients[] = new Patient[10];
//		int i=0;
//		try {
//		Statement statement = connection.createStatement();
//		ResultSet resultSet = statement.executeQuery("select pid,pname,department from patient where pdate IS NULL AND docid IS NULL");
//		while(resultSet.next()) {
//			patients[i] = new Patient();
//			patients[i].setPatientId(resultSet.getString(1));
//			patients[i].setPatientName(resultSet.getString(2));
//			patients[i].setDepartment(resultSet.getString(3));
//		}
//		return patients;
//		}catch(SQLException e) {
//			e.printStackTrace();
//			return null;
//		}

	}

	public boolean schedulePatientDate(String patientId, String department) {

		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("select * from doctor where specialization = '" + department + "'");
			if (resultSet.next()) {
				// resultSet.beforeFirst();

				PreparedStatement preparedStatement = connection
						.prepareStatement("update patient set docid = ?,pdate = ?,status = ? where pid = ?");
				preparedStatement.setString(1, resultSet.getString(1));
				preparedStatement.setString(2, resultSet.getString(4));
				preparedStatement.setBoolean(3, true);
				preparedStatement.setString(4, patientId);
				int status = preparedStatement.executeUpdate();
				if (status != 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

//	public ArrayList getPatientSchedule() {
////		Patient patient = new Patient();
////		Doctor doctor = new Doctor();
//		ArrayList<Object> objectList = new ArrayList<>();
//		Patient patients[] = new Patient[10];
//		Doctor doctors[] = new Doctor[10];
//		int i=0;
//		try {
//			Statement statement = connection.createStatement();
//			ResultSet resultSet = statement.executeQuery("select pid,status,pdate,doctor.docid,ddate from doctor inner join patient where doctor.docid = patient.docid and patient.status =false");
//			while(resultSet.next()) {
//				patients[i] = new Patient();
//				patients[i].setPatientId(resultSet.getString(1));
//				patients[i].setPatientStatus(resultSet.getBoolean(2));
//				patients[i].setPatientDate(resultSet.getString(3));
//				doctors[i] = new Doctor();
//				doctors[i].setDocId(resultSet.getString(4));
//				doctors[i].setDocId(resultSet.getString(5));
//				i++;
//			}
//			objectList.add(patients);
//			
////			objectList[0] = patients;
////			objectList[1] = doctors;
////			patient.setPatientId(resultSet.getString(1));
////			patient.setPatientStatus(resultSet.getBoolean(2));
////			patient.setPatientDate(resultSet.getString(3));
////			doctor.setDocId(resultSet.getString(4));
////			doctor.setDocId(resultSet.getString(5));
////			objectList[0] = patient;
////			objectList[1] = doctor;
//			return objectList;
//		}catch(SQLException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//	

}
