package com.svims.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.svims.database.DatabaseConnection;
import com.svims.model.Patient;
import com.svims.model.Report;

public class ReportDao {
	Connection connection = DatabaseConnection.connectDatabase();

	public boolean createReport(String reportId, String isDiagnosed, String pbs, String cbcs, String antigen,
			String reportPatientid) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					"insert into patientreport(reportId,diagnosed,pbs,cbcs,antigen,pid)values(?,?,?,?,?,?)");
			preparedStatement.setString(1, reportId);
			preparedStatement.setString(2, isDiagnosed);
			preparedStatement.setString(3, pbs);
			preparedStatement.setString(4, cbcs);
			preparedStatement.setString(5, antigen);
			preparedStatement.setString(6, reportPatientid);
			int a = preparedStatement.executeUpdate();
			if (a != 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

	public Report readReport(String reportId) {
		Report report = new Report();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("select * from patientreport where reportId = '" + reportId + "'");
			System.out.println(resultSet);
			resultSet.next();
			report.setReportId(resultSet.getString(1));
			report.setDiagnosed(resultSet.getString(2));
			report.setPbs(resultSet.getString(3));
			report.setCbcs(resultSet.getString(4));
			report.setAntigen(resultSet.getString(5));
			report.setReportPid(resultSet.getString(6));
			return report;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

	public Report[] readReport() {
		Report reports[] = new Report[10];
		int i = 0;
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from patientreport");
			while (resultSet.next()) {
				reports[i] = new Report();
				reports[i].setReportId(resultSet.getString(1));
				reports[i].setDiagnosed(resultSet.getString(2));
				reports[i].setPbs(resultSet.getString(3));
				reports[i].setCbcs(resultSet.getString(4));
				reports[i].setAntigen(resultSet.getString(5));
				reports[i].setReportPid(resultSet.getString(6));
				i++;
			}

			return reports;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
