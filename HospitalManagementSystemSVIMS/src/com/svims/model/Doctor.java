package com.svims.model;

public class Doctor {
	private String docId;
	private String docName;
	private String specialization;
	private String docDate;
	private String docPwd;
	private boolean patientChecked;

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public void setDocDate(String docDate) {
		this.docDate = docDate;
	}

	public void setDocPwd(String docPwd) {
		this.docPwd = docPwd;
	}

	public void setPatientChecked(boolean patientChecked) {
		this.patientChecked = patientChecked;
	}

	public String getDocId() {
		return docId;
	}

	public String getDocName() {
		return docName;
	}

	public String getSpecialization() {
		return specialization;
	}

	public String getDocDate() {
		return docDate;
	}

	public boolean getPatientChecked() {
		return patientChecked;
	}

	public String getDocPwd() {
		return docPwd;
	}

}
