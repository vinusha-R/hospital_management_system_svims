package com.svims.model;

public class Patient {
	private String patientId;
	private String patientName;
	private String department;
	private String patientDate;
	private String patientPwd;
	private boolean checkUpDone;
	private boolean patientStatus;

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPatientDate() {
		return patientDate;
	}

	public void setPatientDate(String patientDate) {
		this.patientDate = patientDate;
	}

	public String getPatientPwd() {
		return patientPwd;
	}

	public void setPatientPwd(String patientPwd) {
		this.patientPwd = patientPwd;
	}

	public boolean isCheckUpDone() {
		return checkUpDone;
	}

	public void setCheckUpDone(boolean checkUpDone) {
		this.checkUpDone = checkUpDone;
	}

	public boolean getPatientStatus() {
		return patientStatus;
	}

	public void setPatientStatus(boolean status) {
		this.patientStatus = status;
	}

}
