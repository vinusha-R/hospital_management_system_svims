package com.svims.model;

public class Report {
	private String reportId;
	private String isDiagnosed;
	private String pbs;
	private String cbcs;
	private String antigen;
	private String reportPid;

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String isDiagnosed() {
		return isDiagnosed;
	}

	public void setDiagnosed(String isDiagnosed) {
		this.isDiagnosed = isDiagnosed;
	}

	public String getPbs() {
		return pbs;
	}

	public void setPbs(String pbs) {
		this.pbs = pbs;
	}

	public String getCbcs() {
		return cbcs;
	}

	public void setCbcs(String cbcs) {
		this.cbcs = cbcs;
	}

	public String getAntigen() {
		return antigen;
	}

	public void setAntigen(String antigen) {
		this.antigen = antigen;
	}

	public String getReportPid() {
		return reportPid;
	}

	public void setReportPid(String reportPid) {
		this.reportPid = reportPid;
	}

}
