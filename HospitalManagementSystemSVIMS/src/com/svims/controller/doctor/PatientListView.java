package com.svims.controller.doctor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.svims.doctor.DoctorOperations;
import com.svims.model.Patient;

/**
 * Servlet implementation class PatientListView
 */
@WebServlet("/viewPatientListAction")
public class PatientListView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientListView() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		DoctorOperations docOperations = new DoctorOperations();
		HttpSession session = request.getSession();
		String doctorId = (String) session.getAttribute("doctorId");
		System.out.println(doctorId);
		// Patient patients[] = new Patient[10];
		// patients = docOperations.viewPatientList(doctorId);
		// System.out.println(patients[0].getPatientId());
		// ArrayList<Patient> arrayList = new ArrayList<>(Arrays.asList(patients));
		// System.out.println(arrayList.get(0).getDepartment());
		ArrayList<Patient> arrayList = docOperations.viewPatientList(doctorId);
		if (arrayList != null) {
			request.setAttribute("patients", arrayList);
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("ViewPatientListJsp");
			reqDispatcher.forward(request, response);
		} else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("PatientNoList.jsp");
			reqDispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
