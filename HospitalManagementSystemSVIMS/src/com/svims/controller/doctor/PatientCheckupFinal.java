package com.svims.controller.doctor;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.svims.admin.AdminOperations;
import com.svims.doctor.DoctorOperations;

/**
 * Servlet implementation class PatientCheckupFinal
 */
@WebServlet("/patientCheckupFinalAction")
public class PatientCheckupFinal extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientCheckupFinal() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String patientId = request.getParameter("patient_id");
		System.out.println(patientId);
		DoctorOperations doctorOperations = new DoctorOperations();
		boolean status = doctorOperations.checkPatient(patientId);
		if (status == true) {
			request.setAttribute("patient_id", patientId);
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("patientCheckupFinalJsp");
			reqDispatcher.forward(request, response);
		} else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("Error.jsp");
			reqDispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
