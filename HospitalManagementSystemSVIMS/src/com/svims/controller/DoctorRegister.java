package com.svims.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.svims.admin.AdminAccount;
import com.svims.doctor.DoctorAccount;
import com.svims.model.Doctor;

/**
 * Servlet implementation class DoctorRegister
 */
@WebServlet("/doctorRegisterAction")
public class DoctorRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DoctorRegister() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		DoctorAccount doctorAccount = new DoctorAccount();
		String docName = request.getParameter("dname");
		String docPassword = request.getParameter("dpwd");
		String specialization = request.getParameter("docSpecialization");
		String docDate = request.getParameter("ddate");
		Doctor doctor = new Doctor();
		doctor.setDocName(docName);
		doctor.setDocPwd(docPassword);
		doctor.setSpecialization(specialization);
		doctor.setDocDate(docDate);
		boolean status = doctorAccount.register(doctor);
		if (status == true) {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("Doctor.html");
			reqDispatcher.forward(request, response);
		} else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("DoctorRegister.html");
			response.getWriter().println("registration failed: Do it again");
			reqDispatcher.include(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
