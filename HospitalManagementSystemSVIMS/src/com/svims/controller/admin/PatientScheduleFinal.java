package com.svims.controller.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.svims.admin.AdminOperations;
import com.svims.model.Patient;

/**
 * Servlet implementation class PatientScheduleFinal
 */
@WebServlet("/patientScheduleFinalAction")
public class PatientScheduleFinal extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientScheduleFinal() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
//		String patientId = request.getParameter("pid");
//		String department = request.getParameter("department");
//		String patientId = (String)request.getAttribute("pid");
//		String department = (String)request.getAttribute("department");
//		HttpSession session = request.getSession();
//		String patientId = (String)session.getAttribute("pid");
//		String department = (String)session.getAttribute("department");

		// request = (HttpServletRequest)session.getAttribute("reqObj");
		// String name= (String)request.getAttribute("name");
		// Patient p = request.getParameter("hiddenValue");
		// Object object= (Object)request.getParameter("hiddenValue");
		// Patient a =(Patient)object;

		String patientId = request.getParameter("patient_id");
		String department = request.getParameter("patient_dept");
		System.out.println("in servlet page");
		System.out.println(patientId);
		System.out.println(department);
		// System.out.println(name);
		AdminOperations adminOperations = new AdminOperations();
		boolean status = adminOperations.schedulePatient(patientId, department);
		if (status == true) {
			request.setAttribute("patient_id", patientId);
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("patientScheduleFinalJsp");
			reqDispatcher.forward(request, response);
		} else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("Error.jsp");
			reqDispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
