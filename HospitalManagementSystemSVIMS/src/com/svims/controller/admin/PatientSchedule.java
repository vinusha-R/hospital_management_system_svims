package com.svims.controller.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.svims.admin.AdminOperations;
import com.svims.model.Patient;

/**
 * Servlet implementation class PatientSchedule
 */
@WebServlet("/schedulePatientAction")
public class PatientSchedule extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientSchedule() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		AdminOperations adminOperations = new AdminOperations();
		// ArrayList objectList = adminOperations.schedulePatient();
//		if (objectList != null) {
//			System.out.println("hello");
//			request.setAttribute("objectList", objectList);
//			RequestDispatcher reqDispatcher = request.getRequestDispatcher("schedulePatientJsp");
//			//RequestDispatcher reqDispatcher = request.getRequestDispatcher("PatientSchedule.jsp");
//			reqDispatcher.forward(request, response);
//		} 
//		Patient patientList[] = adminOperations.schedulePatient();
//		if (patientList != null) {
//			System.out.println("hello");
//			ArrayList<Patient> arrayList = new ArrayList<>(Arrays.asList(patientList));
//			System.out.println(arrayList.get(0).getPatientId());

		ArrayList<Patient> patientList = adminOperations.schedulePatient();
		if (patientList != null) {
			request.setAttribute("patientList", patientList);
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("schedulePatientJsp");
			// RequestDispatcher reqDispatcher =
			// request.getRequestDispatcher("PatientSchedule.jsp");
			reqDispatcher.forward(request, response);
		}

		else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("PatientSchedule.jsp");
			reqDispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
