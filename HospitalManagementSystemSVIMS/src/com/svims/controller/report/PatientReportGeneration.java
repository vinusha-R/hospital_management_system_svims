package com.svims.controller.report;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.svims.doctor.DoctorOperations;

/**
 * Servlet implementation class PatientReportGeneration
 */
@WebServlet("/generatePatientReportAction")
public class PatientReportGeneration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientReportGeneration() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String patientId = request.getParameter("patient_id");
		String isDiagnosed = request.getParameter("patient_diagnosed");
		String pbs = request.getParameter("patient_pbs");
		String cbcs = request.getParameter("patient_cbcs");
		String antigen = request.getParameter("patient_antigen");
		DoctorOperations docOperations = new DoctorOperations();
		boolean status = docOperations.generatePatientReport(isDiagnosed, pbs, cbcs, antigen, patientId);
		if (status == true) {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("patientReportFinalJsp");
			reqDispatcher.forward(request, response);
		} else {
			// error page
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("Error.jsp");
			reqDispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
