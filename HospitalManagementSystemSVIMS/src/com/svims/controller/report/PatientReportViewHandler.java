package com.svims.controller.report;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.svims.doctor.DoctorOperations;
import com.svims.model.Report;

/**
 * Servlet implementation class PatientReportViewHandler
 */
@WebServlet("/viewPatientReportAction")
public class PatientReportViewHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientReportViewHandler() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String reportId = request.getParameter("reportId");
		DoctorOperations doctorOperations = new DoctorOperations();
		Report report = doctorOperations.viewPatientReport(reportId);
//		System.out.println(report.getAntigen());
//		System.out.println(report.getCbcs());
//		System.out.println(report.getPbs());
		if (report != null) {
			request.setAttribute("finalReport", report);
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("patientReportViewAction");
			reqDispatcher.forward(request, response);
		} else {
			// error jsp page
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("Error.jsp");
			reqDispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
