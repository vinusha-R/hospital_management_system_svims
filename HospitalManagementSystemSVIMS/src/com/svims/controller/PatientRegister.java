package com.svims.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.svims.doctor.DoctorAccount;
import com.svims.model.Patient;
import com.svims.patient.PatientAccount;

/**
 * Servlet implementation class PatientRegister
 */
@WebServlet("/patientRegisterAction")
public class PatientRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientRegister() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PatientAccount patientAccount = new PatientAccount();
		String patientName = request.getParameter("pname");
		String patientPassword = request.getParameter("ppwd");
		System.out.println(patientPassword);
		String patientDept = request.getParameter("pdepartment");
		Patient patient = new Patient();
		patient.setPatientName(patientName);
		patient.setPatientPwd(patientPassword);
		patient.setDepartment(patientDept);
		boolean status = patientAccount.register(patient);
		if (status == true) {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("Patient.html");
			reqDispatcher.forward(request, response);
		} else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("PatientRegister.html");
			response.getWriter().println("registration failed: Do it again");
			reqDispatcher.include(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
