package com.svims.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.svims.admin.AdminAccount;
import com.svims.model.Admin;

/**
 * Servlet implementation class AdminRegister
 */
@WebServlet("/adminRegisterAction")
public class AdminRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminRegister() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		AdminAccount adminAccount = new AdminAccount();
		String adminName = request.getParameter("aname");
		String adminPwd = request.getParameter("apwd");
		Admin admin = new Admin();
		admin.setAdminName(adminName);
		admin.setAdminPwd(adminPwd);
		boolean status = adminAccount.register(admin);
		if (status == true) {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("Admin.html");
			reqDispatcher.forward(request, response);
		} else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("AdminRegister.html");
			response.getWriter().println("registration failed: Do it again");
			reqDispatcher.include(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
