package com.svims.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.svims.UserAccount;
import com.svims.admin.AdminAccount;
import com.svims.doctor.DoctorAccount;

/**
 * Servlet implementation class DoctorLogin
 */
@WebServlet("/doctorLoginAction")
public class DoctorLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DoctorLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String doctorId = request.getParameter("did");
		String doctorPassword = request.getParameter("dpwd");

		UserAccount doctorAccount = new DoctorAccount();
		boolean status = doctorAccount.login(doctorId, doctorPassword);
		if (status == true) {
			HttpSession session = request.getSession();
			session.setAttribute("doctorId", doctorId);
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("Doctor.html");
			reqDispatcher.forward(request, response);
		} else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("DoctorLogin.html");
			response.getWriter().println("Invalid login id or password");
			reqDispatcher.include(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
