package com.svims.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.svims.UserAccount;
import com.svims.patient.PatientAccount;

/**
 * Servlet implementation class PatientLogin
 */
@WebServlet("/patientLoginAction")
public class PatientLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String patientId = request.getParameter("pid");
		String patientPassword = request.getParameter("ppwd");
		UserAccount patientAccount = new PatientAccount();
		boolean status = patientAccount.login(patientId, patientPassword);
		if (status == true) {
			HttpSession session = request.getSession();
			session.setAttribute("patientId", patientId);
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("Patient.html");
			reqDispatcher.forward(request, response);
		} else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("PatientLogin.html");
			response.getWriter().println("Invalid login id or password");
			reqDispatcher.include(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
