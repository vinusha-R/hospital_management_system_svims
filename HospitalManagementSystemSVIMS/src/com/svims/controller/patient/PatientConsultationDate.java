package com.svims.controller.patient;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.svims.model.Patient;
import com.svims.patient.PatientOperations;

/**
 * Servlet implementation class PatientConsultationDate
 */
@WebServlet("/patientConsultationDateAction")
public class PatientConsultationDate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientConsultationDate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PatientOperations patientOperations = new PatientOperations();
		HttpSession session = request.getSession();
		String patientId = (String) session.getAttribute("patientId");
		Patient patient = patientOperations.checkConsultationDate(patientId);

		if (patient != null) {
			request.setAttribute("patientDate", patient.getPatientDate());
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("patientConsultationDateJsp");
			reqDispatcher.forward(request, response);
		} else {
			request.setAttribute("patientDate", "Your consultation date is not yet assigned");
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("patientConsultationDateJsp");
			reqDispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
