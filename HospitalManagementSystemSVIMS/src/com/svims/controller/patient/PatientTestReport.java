package com.svims.controller.patient;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.svims.model.Report;
import com.svims.patient.PatientOperations;

/**
 * Servlet implementation class PatientTestReport
 */
@WebServlet("/viewTestReportAction")
public class PatientTestReport extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PatientTestReport() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PatientOperations patientOperations = new PatientOperations();
		HttpSession session = request.getSession();
		String patientId = (String) session.getAttribute("patientId");
		Report report = patientOperations.viewTestReport(patientId);
		if (report != null) {
			request.setAttribute("finalReport", report);
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("patientReportViewAction");
			reqDispatcher.forward(request, response);
		} else {
			RequestDispatcher reqDispatcher = request.getRequestDispatcher("PatientNoReport.jsp");
			reqDispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
