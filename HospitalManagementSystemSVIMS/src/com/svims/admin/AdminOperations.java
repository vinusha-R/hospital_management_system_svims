package com.svims.admin;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.svims.dao.AdminDao;
import com.svims.model.Patient;

public class AdminOperations {
	AdminDao adminDao = new AdminDao();

//	public ArrayList schedulePatient(){
//		ArrayList objectList= adminDao.getPatientSchedule();
//		return objectList;
//	}
	public ArrayList<Patient> schedulePatient() {
		ArrayList<Patient> patientList = adminDao.getPatientSchedule();
		return patientList;
	}

	public boolean schedulePatient(String patientId, String department) {
		boolean status = adminDao.schedulePatientDate(patientId, department);
		return status;
	}
}
