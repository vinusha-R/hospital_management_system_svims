package com.svims.admin;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import com.svims.UserAccount;
import com.svims.dao.AdminDao;
import com.svims.model.Admin;

public class AdminAccount implements UserAccount {
	AdminDao adminDao = new AdminDao();

	// ArrayList<Admin> admins = new Admin[10];
	@Override
	public boolean login(String adminId, String adminPassword) {
		// TODO Auto-generated method stub
		// ArrayList<String> adminIdList = new ArrayList();

		String password = null;

		ArrayList<String> idList = adminDao.readAdmin("admin");
//			int counter = 0;
//			for (int i = 0; i < admins.length; i ++) {
//			    if (admins[i] != null)
//			        counter ++;
//			}
//			System.out.println(counter);
//			for(int i=0;i<admins.length; i++) {
//			    if (admins[i] != null) {
//					System.out.println(admins[i]);
//					adminIdList.add(admins[i].getAdminId());
//
//			    }
//			    	
//			}
		if (idList.contains(adminId)) {
			Admin admin = adminDao.readAdmin("admin", adminId);
			password = admin.getAdminPwd();
		} else {
			return false;
		}
		if (password.equals(adminPassword)) {
			System.out.println("welcome");
			return true;
		} else {
			System.out.println("invalid password");
			return false;
		}

	}

	@Override
	public boolean register(Object object) {
		Admin admin = (Admin) object;
		String adminName = admin.getAdminName();
		String adminPassword = admin.getAdminPwd();
		int id = generateId("admin");
		String newId = "a" + id;
		boolean status = adminDao.createAdmin(newId, adminName, adminPassword);
		return status;
	}

	@Override
	public int generateId(String tableName) {
//			Admin admins[] = new Admin[10];
//			admins = adminDao.readAdmin(tableName);
//			ArrayList<String> adminIdList = new ArrayList();
//			if(admins == null) {
//				return 1;
//			}
//			else {
//				for(int i=0;i<admins.length; i++) {
//				    if (admins[i] != null) {
//						System.out.println(admins[i]);
//						adminIdList.add(admins[i].getAdminId());
//				    }
//				    	
//				}	
//			String id = adminIdList.get(adminIdList.size()-1);
//			int newId = Integer.parseInt(id.substring(1));
//			return ++newId;
//			}

		ArrayList<String> idList = adminDao.readAdmin(tableName);
		System.out.println(idList);
		if (idList.isEmpty()) {
			return 1;
		} else {
			String id = idList.get(idList.size() - 1);
			int newId = Integer.parseInt(id.substring(1));
			return ++newId;
		}
	}
}
