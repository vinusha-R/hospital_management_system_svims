package com.svims.patient;

import java.util.ArrayList;

import com.svims.UserAccount;
import com.svims.dao.DoctorDao;
import com.svims.dao.PatientDao;
import com.svims.model.Doctor;
import com.svims.model.Patient;

public class PatientAccount implements UserAccount {
	PatientDao patientDao = new PatientDao();
	Patient patients[] = new Patient[10];

	@Override
	public boolean login(String patientId, String patientPassword) {
		// PatientDao patientDao = new PatientDao();
		// Patient patients[] = new Patient[10];

		// TODO Auto-generated method stub
		// ArrayList<String> patientIdList = new ArrayList<>();

		String password = null;

		ArrayList<String> idList = patientDao.readPatient("patient");

		if (idList.contains(patientId)) {
			Patient patient = patientDao.readPatient("patient", patientId);
			password = patient.getPatientPwd();
		} else {
			return false;
		}
		if (password.equals(patientPassword)) {
			System.out.println("welcome");
			return true;
		} else {
			System.out.println("invalid password");
			return false;
		}

	}

	@Override
	public boolean register(Object object) {
		Patient patient = (Patient) object;
		String patientName = patient.getPatientName();
		String patientPassword = patient.getPatientPwd();
		String patientDept = patient.getDepartment();

		boolean patientChecked = false;
		int id = generateId("patient");
		String newId = "p" + id;
		String docid = null;
		String patientDate = null;
		Boolean status = false;
		Boolean checkupDone = false;

		boolean setStatus = patientDao.createPatient(newId, patientName, patientDept, docid, patientPassword,
				patientDate, status, checkupDone);
		return setStatus;

	}

	@Override
	public int generateId(String tableName) {
		// Patient patients[] = new Patient[10];
		ArrayList<String> idList = patientDao.readPatient(tableName);
		if (idList.isEmpty()) {
			return 1;
		} else {
			String id = idList.get(idList.size() - 1);
			int newId = Integer.parseInt(id.substring(1));
			return ++newId;
		}
	}

}