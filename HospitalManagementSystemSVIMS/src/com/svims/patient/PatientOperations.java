package com.svims.patient;

import com.svims.dao.PatientDao;
import com.svims.dao.ReportDao;
import com.svims.model.Patient;
import com.svims.model.Report;

public class PatientOperations {
	PatientDao patientDao = new PatientDao();

	public Patient checkConsultationDate(String patientId) {
		Patient patient = patientDao.readPatient("patient", patientId);
		if (patient != null) {
			if (patient.getPatientStatus() == true) {
				return patient;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public Report viewTestReport(String patientId) {
		ReportDao reportDao = new ReportDao();
		Report report = reportDao.readReport(patientId);
		if (report != null) {
			return report;
		} else {
			return null;
		}

	}
}
