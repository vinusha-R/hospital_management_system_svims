package com.svims;

public interface UserAccount {
	boolean login(String id, String password);
	boolean register(Object object);
	int generateId(String tableName);
}
